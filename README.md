# OnTrack App Test

## Introduction

The front-end code for the OnTrack App test lending app. Built in [React](https://reactjs.org/).

## Requirements

- [Node](https://nodejs.org/en/).
- [npm](https://www.npmjs.com/package/npm).

## Getting Started

**1. Clone Git Repo.**

```
$ git clone git clone https://kkbokyere@bitbucket.org/kkbokyere/books-challenge.git
```

**2. Install Dependencies.**

Once that's all done, cd into the weather-forecaster web directory and install the depedencies:

```
$ cd books-challenge/web
$ yarn install
```

**3. Run Application.**

Once the node modules have all been installed and npm has done it's thing, that's it. To open up a local development environment, run:

```
$ yarn start
```

Once the server is up and running, navigate to [localhost:3000](http://localhost:3000).

## Testing

[Jest](https://jestjs.io/) is the test runner used, with [React Testing Library](https://testing-library.com/docs/react-testing-library/) is testing library used for testing components. To run test use the following command:

```
$ yarn test
```

## Deployment

No CI/CD pipeline at the moment.

# Tools Used

- [React](https://reactjs.org/)
- [Create React App](https://create-react-app.dev/)
- [Redux](https://redux.js.org)
- [Redux Thunk](https://github.com/reduxjs/redux-thunk)
- [CSS Modules](https://github.com/css-modules/css-modules)
- [Material UI](https://material-ui.com/)

# Improvements / Retrospective Review

- Would have used Cypress for E2E testing
- 100% test coverage
- create a better Error handler - only catching one simple error scenario
- implement typescript to better type definitions
- Would have considered creating a proper [SMACSS](http://smacss.com/) architecture for base CSS styles such as layout. 
- better styling
