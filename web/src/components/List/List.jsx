import React, {useEffect} from 'react';
import PropTypes from 'prop-types'

import styles from './List.module.scss'
import ListItem from "@components/ListItem";
import {CircularProgress} from "@material-ui/core";
import Pagination from "@material-ui/lab/Pagination";

const List = ({ data = [], isLoading, handleFetchBooks = () => {}, page, count }) => {
    const [pageParam, setPageParam] = React.useState(page);
    const handleChange = (event, value) => {
        setPageParam(value);
    };

    useEffect(() => {
        handleFetchBooks({ page: pageParam })
    }, [pageParam]);
    if(isLoading) {
        return <span data-testid="loading-spinner"><CircularProgress/></span>
    }
    return (
        <div>
        <div className={styles.listContainer} data-testid="list-container">
            {data && data.map(item => <ListItem key={item.id} data={item}/>)}
        </div>
            <Pagination count={count} page={pageParam} onChange={handleChange} />
        </div>
    );
};

List.propTypes = {
    data: PropTypes.array,
};

export default React.memo(List);
