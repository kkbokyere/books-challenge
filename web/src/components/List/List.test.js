import React from 'react';
import { render } from '@tests/helper'

import List from '@components/List';
import {mockResponse} from "../../tests/__mocks__/data";
describe('List Tests', () => {

  const setup = (props) => {
    return render(<List {...props}/>);
  };
  it('should render List', async () => {
    const { asFragment } = setup();

    expect(asFragment()).toMatchSnapshot();
  });

  it('should render List with 1 items', async () => {
    const { asFragment, getByText, getByTestId } = setup({
      data: mockResponse.books
    });
    expect(getByTestId('list-container').children.length).toBe(1);
    expect(asFragment()).toMatchSnapshot();
  });

  it('should render List with multiple items', async () => {
    const { asFragment, getByText, getByTestId } = setup({
      data: mockResponse.books
    });
    expect(getByTestId('list-container').children.length).toBe(1);
    expect(getByText('Book Title')).toBeInTheDocument();
    expect(asFragment()).toMatchSnapshot();
  });

  it('should render List with multiple items', async () => {
    const fetchBooksMock = jest.fn();
    const { asFragment, getByText, getByTestId } = setup({
      data: mockResponse.books,
      handleFetchBooks: fetchBooksMock
    });
    expect(getByTestId('list-container').children.length).toBe(1);
    expect(getByText('Book Title')).toBeInTheDocument();
    expect(asFragment()).toMatchSnapshot();
    expect(fetchBooksMock).toHaveBeenCalledWith({ page: 1})
  });
});
