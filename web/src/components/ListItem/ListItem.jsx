import React from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';

import styles from './ListItem.module.scss'

const ListItem = ({ data }) => {
    return (
        <Card className={styles.listItem}>
            <h3 className={styles.listItemTitle}>{data.book_title}</h3>
        </Card>
    );
};

ListItem.propTypes = {
    data: PropTypes.object
};

export default ListItem;
