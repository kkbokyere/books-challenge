import React from 'react';
import { render } from '@tests/helper'

import ListItem from '@components/ListItem';
import {mockResponse} from "../../tests/__mocks__/data";
describe('ListItem Tests', () => {


  const setup = (props) => {
    return render(<ListItem {...props}/>);
  };
  it('should render ListItem', async () => {
    const { asFragment } = setup({
      data: {}
    });

    expect(asFragment()).toMatchSnapshot();
  });

  it('should render ListItem with data', async () => {
    const { asFragment, getByText } = setup({
      data: mockResponse.books[0]
    });
    expect(getByText('Book Title')).toBeInTheDocument();
    expect(asFragment()).toMatchSnapshot();
  });
});
