import React from 'react';
import {
    useLocation
} from "react-router-dom";

import '@styles/App.scss';
import Layout from "@components/Layout";
import BooksList from "@containers/BooksList";


function useQuery() {
    return new URLSearchParams(useLocation().search);
}

function App() {
    const query = useQuery();
    const pageParam = Number(query.get('page')) || 1;
    const itemsPerPageParam = Number(query.get('itemsPerPage')) || 20;
    const filtersParam = query.get('filters') || [];
  return (
    <div className="App">
      <Layout>
        <h1>Books</h1>
        <BooksList page={pageParam} itemsPerPage={itemsPerPageParam} filters={filtersParam}/>
      </Layout>
    </div>
  );
}

export default App;
