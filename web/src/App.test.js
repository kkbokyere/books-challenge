import React from 'react';
import axiosMock from "axios";
import { render, fireEvent, cleanup, waitForElement } from './tests/helper'
import App from './App';
import {mockResponse} from "./tests/__mocks__/data";

describe('App Tests', () => {
  afterEach(cleanup);
  const setup = (props) => {
    return render(<App/>, props);
  };

  it('should render app with loading state', () => {
    const { asFragment, getByText, getByTestId } = setup();

    expect(asFragment()).toMatchSnapshot();
    expect(getByText('Books')).toBeInTheDocument();
    expect(getByTestId('loading-spinner')).toBeInTheDocument();
  });

  it('should render app with data', async () => {
    axiosMock.post
        .mockResolvedValueOnce({ data: mockResponse });

    const { asFragment, getByText, getByTestId } = setup();

    const updatedResolvedList = await waitForElement(() => getByTestId("list-container"));

    expect(updatedResolvedList.children.length).toBe(1);

  });


  it('should render app with data on page 2', async () => {
    axiosMock.post
        .mockResolvedValueOnce({ data: mockResponse });

    const { asFragment, getByText, getByTestId } = setup({
      route: '/?page=2'
    });

    const updatedResolvedList = await waitForElement(() => getByTestId("list-container"));

    expect(updatedResolvedList.children.length).toBe(1);

  });
});
