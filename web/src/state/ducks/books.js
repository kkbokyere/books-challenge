import {getBooks} from "@api/books";
export const GET_BOOKS_SUCCESS = "GET_BOOKS_SUCCESS";
export const GET_BOOKS_FAILURE = "GET_BOOKS_FAILURE";
export const GET_BOOKS_REQUEST = "GET_BOOKS_REQUEST";

export const initialState = {
    isLoading: false,
    error: null,
    data: []
};

function getBooksRequest(payload) {
    return {
        type: GET_BOOKS_REQUEST,
        payload
    }
}

function getBooksSuccess(payload) {
    return {
        type: GET_BOOKS_SUCCESS,
        payload
    }
}

function getBooksFailure(payload) {
    return {
        type: GET_BOOKS_FAILURE,
        payload
    }
}

export function fetchBooks(params) {
    return function (dispatch) {
        dispatch(getBooksRequest(params));
        return getBooks(params)
            .then(response => {
                dispatch(getBooksSuccess(response))
            }).catch(error => {
                dispatch(getBooksFailure(error.response.data))
            })
    }
}

export default (state = initialState, action) => {
    let { payload } = action;
    switch (action.type) {
        case GET_BOOKS_SUCCESS:
            return {
                ...state,
                data: payload,
                isLoading: false
            };
        case GET_BOOKS_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: payload
            };
        case GET_BOOKS_REQUEST:
            return {
                ...state,
                error: null,
                isLoading: true
            };
        default:
            return state
    }
}
