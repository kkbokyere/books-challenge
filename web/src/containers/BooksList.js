import { connect } from 'react-redux'
import List from "@components/List";
import {fetchBooks} from "../state/ducks/books";
const mapStateToProps = ({ books }, ownProps) => {
    return {
        data: books.data && books.data.books,
        count: books.data && books.data.count,
        isLoading: books.isLoading,
        ...ownProps
    }
};

const mapDispatchToProps = (dispatch, { page = 1, itemsPerPage = 20, filters = [] }) => {
    return {
        handleFetchBooks: (params) => dispatch(fetchBooks({itemsPerPage, filters, page, ...params}))
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(List)
