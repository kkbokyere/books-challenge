export const mockResponse = {
    books: [{
        book_author: ["Some Author"],
        book_pages: 770,
        book_publication_city: "London",
        book_publication_country: "UK",
        book_publication_year: 1631,
        book_title: "Book Title",
        id: 2147
    }]
};
