import * as React from "react";
import {Provider} from 'react-redux'
import { Router } from 'react-router-dom'
import { createMemoryHistory } from 'history'
import {render as rtlRender } from '@testing-library/react'
import configureStore from "@state/store";

const initialReducerState = {};


function render(
    ui,
    {
        route = '/',
        history = createMemoryHistory({ initialEntries: [route] }),
        initialState = initialReducerState,
        ...renderOptions
    } = {},
) {
    function Wrapper({children}) {
        return (
            <Provider store={configureStore(initialState)}>
                <Router history={history}>
                    {children}
                </Router>
            </Provider>
        )
    }
    return rtlRender(ui, {wrapper: Wrapper, ...renderOptions})
}

// re-export everything
export * from '@testing-library/react'

// override render method
export { render }
