import axios from 'axios'
export const API_URL = process.env.REACT_APP_API_URL;

export const getBooks = ({ page = 1, itemsPerPage = 20, filters = []}) => {
    return axios.post(`${API_URL}/books`, {
        page,
        itemsPerPage,
        filters
    })
        .then(response => {
            return response.data
        })
};
